<?php

use Illuminate\Support\Facades\Route;

Route::get('items/available/{minAmount?}', 'ItemController@available');
Route::get('items/not_available', 'ItemController@notAvailable');
Route::apiResource('items', 'ItemController');
