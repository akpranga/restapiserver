<?php

namespace AKPranga\ApiServer\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Item
 * @package AKPranga\ApiServer\Models
 * @mixin \Eloquent
 */
class Item extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name',
        'amount'
    ];
}
