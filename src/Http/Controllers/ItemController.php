<?php

namespace AKPranga\ApiServer\Http\Controllers;

use AKPranga\ApiServer\Http\Requests\ItemRequest;
use AKPranga\ApiServer\Models\Item;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller;

class ItemController extends Controller
{
    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $items = Item::all();

        return response()->json($items);
    }

    /**
     * @param ItemRequest $request
     *
     * @return JsonResponse
     */
    public function store(ItemRequest $request): JsonResponse
    {
        $item = new Item();
        $item->fill($request->toArray());
        $item->save();

        return response()->json($item);
    }

    /**
     * @param Item $item
     *
     * @return JsonResponse
     */
    public function show(Item $item): JsonResponse
    {
        return response()->json($item);
    }

    /**
     * @param Item $item
     * @param ItemRequest $request
     *
     * @return JsonResponse
     */
    public function update(Item $item, ItemRequest $request): JsonResponse
    {
        $item->fill($request->toArray());
        $item->save();

        return response()->json($item);
    }

    /**
     * @param Item $item
     *
     * @return JsonResponse
     */
    public function destroy(Item $item): JsonResponse
    {
        $item->delete();

        return response()->json();
    }

    /**
     * @param int $minAmount
     *
     * @return JsonResponse
     */
    public function available(int $minAmount = 1): JsonResponse
    {
        $items = Item::where('amount', '>', $minAmount)->get();

        return response()->json($items);
    }

    /**
     * @return JsonResponse
     */
    public function notAvailable(): JsonResponse
    {
        $items = Item::where('amount', '=', 0)->get();

        return response()->json($items);
    }
}
