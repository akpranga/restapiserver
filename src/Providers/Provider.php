<?php

namespace AKPranga\ApiServer\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Route;

/**
 * Class Provider
 *
 * @package BmApiServer\Providers
 */
class Provider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     */
    public function register(): void
    {
        $this->registerResources();
        $this->registerRoutes();
    }

    /**
     * @return void
     */
    public function registerResources(): void
    {
        $this->loadViewsFrom(__DIR__ . '/../../resources/views', 'bm-api-view');
        $this->loadMigrationsFrom(__DIR__ . '/../../database/migrations');
    }

    private function registerRoutes(): void
    {
        Route::group([
            'prefix'     => 'api',
            'namespace'  => 'AKPranga\ApiServer\Http\Controllers',
            'middleware' => 'api',
        ], function () {
            $this->loadRoutesFrom(__DIR__ . '/../../routes/api.php');
        });
    }

    /**
     * Perform post-registration booting of services.
     * @return void
     */
    public function boot(): void
    {
        //
    }
}
