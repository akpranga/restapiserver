RestApi Server Package for Laravel 5
====================================

Requirments
====================================
* PHP >= 7.1
* cURL Extension
* Laravel >= 5.6

Installation
============

        composer require akpranga/restapiserver
        
After install you must run migrations 

        php artisan migrate
Credits
=======
* Adrian Pranga